Selective Content Block module
-----------------
by Waldemar Raimundo Krumrick, waldemar.krumrick@darwoft.com ,  www.darwoft.com


This is a block module. You only need to install it and then go to the block 
list and choose where you want to show the block.

--------------------------------------------------------------------------------
                                Selective Content Block Module
--------------------------------------------------------------------------------

The block shows a couple of links that once clicked, load data with ajax.
The data to load are all configurable through sql sentences. 
The idea to have sql sentences is to provide to developers a powerfull without
restrictions tool to find and show data.
For example, it is a good way to:
  - make a glosary.
  - show in one block different kind of content type: articles, pages, some 
specific content that the user previously can create such as companies, 
students, and so on.
  
The main reason to use sql to show content is because we can use complex 
sentences with operators like:
 concat, regular expression comparison, sum, average, ..... having, group by. 
No limit in what we can write on sql sentences.

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($){

  /* applies the tpl (a selector of a template) to the data and puts it as the html of the object
   * it triggers two events
   * assign (to be able to alter the data)
   * render (after the html has been added to the dom) to allow jquery event initialisations
   * options.method = html (or after or before or any html injection jquery method
   *
   */
  $("#selective_content_block-filter-1").live("click", function(){
    $('#selective_content_block-body-find').empty();
      // Drupal.settings.path_module_selective_content.path   // module path.

      $('#selective_content_block_loading').css('display', 'inline-block');
      var urls = location.protocol + "//" + location.host + Drupal.settings.base_path.path + "selective_content_block/content";
      $.ajax({
                    type: 'POST',
                    
                    url: urls,
                    data: {filter: 1},
                    cache: false,
                    success: function(data){
                            for (var i=1; data[i] != null; i++) {
                                $('#selective_content_block_loading').css('display', 'none');
                                $('#selective_content_block-body-find').append(data[i]['data']);
                            }
                           
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                            alert(xhr.status);
                            alert(thrownError);
                             $('#selective_content_block_loading').css('display', 'none');
                    }       
                });
      

     
  
  }); 
  
 
  
  function er (data) {
    // Parse Json
    // Add some stuff to your DOM.
    alert('ERROR'+data);
  }
  
  $("#selective_content_block-filter-2").live("click", function(){
      $('#selective_content_block-body-find').empty();
      $('#selective_content_block_loading').css('display', 'inline-block');
     var urls = location.protocol + "//" + location.host + Drupal.settings.base_path.path + "selective_content_block/content";
      $.ajax({
                    type: 'POST',
                    url: urls,
                    data: {filter: 2},
                    cache: false,
                    success: function(data){
                            for (var i=1; data[i] != null; i++) {
                                $('#selective_content_block_loading').css('display', 'none');
                                $('#selective_content_block-body-find').append(data[i]['data']);
                            }
                           
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                            alert(xhr.status);
                            alert(thrownError);
                            $('#selective_content_block_loading').css('display', 'none');
                    }       
                });
  }); 
  
  
  $("#selective_content_block-filter-3").live("click", function(){
      $('#selective_content_block-body-find').empty();
      $('#selective_content_block_loading').css('display', 'inline-block');
      var urls = location.protocol + "//" + location.host + Drupal.settings.base_path.path + "selective_content_block/content";
      $.ajax({
                    type: 'POST',
                    url: urls,
                    data: {filter: 3},
                    cache: false,
                    success: function(data){
                            for (var i=1; data[i] != null; i++) {
                                $('#selective_content_block_loading').css('display', 'none');
                                $('#selective_content_block-body-find').append(data[i]['data']);
                            }
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                            alert(xhr.status);
                            alert(thrownError);
                            $('#selective_content_block_loading').css('display', 'none');
                    }
                });
  });
  
   $("#selective_content_block-filter-4").live("click", function(){
      $('#selective_content_block-body-find').empty();
      $('#selective_content_block_loading').css('display', 'inline-block');
      var urls = location.protocol + "//" + location.host + Drupal.settings.base_path.path + "selective_content_block/content";
      $.ajax({
                    type: 'POST',
                    url: urls,
                    data: {filter: 4},
                    cache: false,
                    success: function(data){
                            for (var i=1; data[i] != null; i++) {
                                $('#selective_content_block_loading').css('display', 'none');
                                $('#selective_content_block-body-find').append(data[i]['data']);
                            }
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                            alert(xhr.status);
                            alert(thrownError);
                            $('#selective_content_block_loading').css('display', 'none');
                    }
                });
  });
})(jQuery);


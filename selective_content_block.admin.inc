<?php

function selective_content_block_admin($p_nid = -1) {
  $content = drupal_render((drupal_get_form('selective_content_block_config_form')));
  return $content;
}

function selective_content_block_config_form() {
   
  if (isset($_POST['title_link'])) {
    $v_titleLink = $_POST['title_link'];
    $v_urlLink = $_POST['url_link'];
    $v_title = $_POST['title'];
    $v_description = $_POST['description'];
    $v_filter1Label = $_POST['filter_1_label'];
    $v_filter1Sql = $_POST['filter_1_sql'];
    $v_filter2Label = $_POST['filter_2_label'];
    $v_filter2Sql = $_POST['filter_2_sql'];
    $v_filter3Label =$_POST['filter_3_label'];
    $v_filter3Sql = $_POST['filter_3_sql'];
    $v_filter4Label = $_POST['filter_4_label'];
    $v_filter4Sql = $_POST['filter_4_sql'];
    $v_titleLinkBottom = $_POST['title_link_bottom'];
    $v_urlLinkBottom = $_POST['url_link_bottom'];
    $v_query = $_POST['query'];
    //$v_fields = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '16'))->fetchAssoc();

  } else {
    $v_titleLink = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '1'))->fetchAssoc();  
    $v_urlLink = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '2'))->fetchAssoc();
    $v_title = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '3'))->fetchAssoc();
    $v_description = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '4'))->fetchAssoc();
    $v_description = $v_description['value'];
    $v_filter1Label = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '5'))->fetchAssoc();
    $v_filter1Sql = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '6'))->fetchAssoc();
    $v_filter1Sql = $v_filter1Sql['value'];
    $v_filter2Label = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '7'))->fetchAssoc();
    $v_filter2Sql = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '8'))->fetchAssoc();
    $v_filter2Sql = $v_filter2Sql['value'];
    $v_filter3Label = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '9'))->fetchAssoc();
    $v_filter3Sql = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '10'))->fetchAssoc();
    $v_filter3Sql = $v_filter3Sql['value'];
    $v_filter4Label = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '11'))->fetchAssoc();
    $v_filter4Sql = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '12'))->fetchAssoc();
    $v_filter4Sql = $v_filter4Sql['value'];
    $v_titleLinkBottom = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '13'))->fetchAssoc();
    $v_urlLinkBottom = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '14'))->fetchAssoc();
    $v_query = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '15'))->fetchAssoc();
    $v_query = $v_query['value'];
    $v_fields = db_query("SELECT value FROM {selective_content_block} WHERE nid=:nid",array(':nid' => '16'))->fetchAssoc();
  }     
  $form['title_link'] = array(
    '#type' => 'textfield',
    '#id' => 'title_link',
    '#title' => t('Title Link'),
    '#value' => $v_titleLink,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The label for the title link'),
    '#required' => TRUE,
  );
  $form['url_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Title URL Link'),
    '#value' => $v_urlLink,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The url for the title link'),
    '#required' => TRUE,
  );
   $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#value' => $v_title,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The title for the block'),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#value'=> $v_description,
    
    '#rows' => 5,
    '#description' => t('The description for the block'),
    '#required' => TRUE,
  );
  
  $form['filter_1_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter 1 Label'),
    '#value' => $v_filter1Label,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The label for filter 1'),
    '#required' => TRUE,
  );
  $form['filter_1_sql'] = array(
    '#type' => 'textarea',
    '#title' => t('Filter 1 SQL'),
    '#value' => $v_filter1Sql,
   
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The sql for filter 1'),
    '#required' => TRUE,
  );
  $form['filter_2_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter 2 Label'),
    '#value' => $v_filter2Label,
   
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The label for filter 2'),
    '#required' => TRUE,
  );
  $form['filter_2_sql'] = array(
    '#type' => 'textarea',
    '#title' => t('Filter 2 SQL'),
    '#value' => $v_filter2Sql,
   
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The sql for filter 2'),
    '#required' => TRUE,
  );
  $form['filter_3_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter 3 Label'),
    '#value' => $v_filter3Label,
    
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The label for filter 3'),
    '#required' => TRUE,
  );
  $form['filter_3_sql'] = array(
    '#type' => 'textarea',
    '#title' => t('Filter 3 SQL'),
    '#value' => $v_filter3Sql,
    
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The sql for filter 3'),
    '#required' => TRUE,
  );
  $form['filter_4_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Filter 4 Label'),
    '#value' => $v_filter4Label,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The label for filter 4'),
    '#required' => TRUE,
  );
  $form['filter_4_sql'] = array(
    '#type' => 'textarea',
    '#title' => t('Filter 4 SQL'),
    '#value' => $v_filter4Sql,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The sql for filter 4'),
    '#required' => TRUE,
  );
  
  $form['title_link_bottom'] = array(
    '#type' => 'textfield',
    '#title' => t('Title Bottom Link'),
    '#value' => $v_titleLinkBottom,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The title for bottom link'),
    '#required' => TRUE,
  );
  $form['url_link_bottom'] = array(
    '#type' => 'textfield',
    '#title' => t('Url Bottom Link'),
    '#value' => $v_urlLinkBottom,
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The url for bottom link'),
    '#required' => TRUE,
  );
  $form['query'] = array(
    '#type' => 'textarea',
    '#title' => t('Main query'),
    '#value' => $v_query,
    '#cols' => 60,
    '#maxlength' => 500,
    '#description' => t("Main query to look for content. You should not include \'where\' conditions. Your query must select three values (alias: pos1, ref1 and pos2). This values will be used to show a title (link) and a content."),
    '#required' => TRUE,
  );
  $form['fields'] = array(
    '#type' => 'textfield',
    '#title' => t('Fields'),
    '#text' => 'Not implemented yet',
    '#size' => 120,
    '#maxlength' => 500,
    '#description' => t('The fields to show (Not implemented yet)'),
    '#required' => FALSE,
    '#disabled' => 'disabled',
  );  

    $form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Save'),
	
    );
  
    return $form;
}

/**
 * Submit
 * @param <type> $form
 * @param <type> $form_state
 * @return <type>
 */function selective_content_block_config_form_submit($form, &$form_state) {
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '1',':value' => $_POST['title_link']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '2',':value' => $_POST['url_link']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '3',':value' => $_POST['title']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '4',':value' => $_POST['description']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '5',':value' => $_POST['filter_1_label']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '6',':value' => $_POST['filter_1_sql']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '7',':value' => $_POST['filter_2_label']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '8',':value' => $_POST['filter_2_sql']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '9',':value' => $_POST['filter_3_label']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '10',':value' => $_POST['filter_3_sql']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '11',':value' => $_POST['filter_4_label']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '12',':value' => $_POST['filter_4_sql']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '13',':value' => $_POST['title_link_bottom']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '14',':value' => $_POST['url_link_bottom']));
      db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '15',':value' => $_POST['query']));
    //  db_query("UPDATE {selective_content_block} set value = :value where nid = :nid",array(':nid' => '16',':value' => $_POST['fields']));
      return;
}/**
* Implements validation from the Form API.
*
* @param $form
*   A structured array containing the elements and properties of the form.
* @param $form_state
*   An array that stores information about the form's current state
*   during processing.
*/
function selective_content_block_config_form_validate($form, &$form_state) {

  $error1 = $_POST['title_link'];
  if ($error1 == null || $error1 == "" ) {
    form_set_error('title_link', t('The title link is required'));
  }
  $error2 = $_POST['url_link'];
  if ($error2 == "" ) {
    form_set_error('url_link', t('The url link is required'));
  }
  
  $error3 = $_POST['title'];
  if ($error3 == "" ) {
    form_set_error('title', t('The title is required'));
  }
  $error4 = $_POST['description'];
  if ($error4 == "" ) {
    form_set_error('description', t('The description is required'));
  }
  
  $error5 = $_POST['filter_1_label'];
  if ($error5 == "" ) {
    form_set_error('filter_1_label', t('The label for filter 1 is required'));
  }
  $error6 = $_POST['filter_1_sql'];
  if ($error6 == "" ) {
    form_set_error('filter_1_sql', t('The sql for filter 1 is required'));
  }
  
  $error7 = $_POST['filter_2_label'];
  if ($error7 == "" ) {
    form_set_error('filter_2_label', t('The label for filter 2 is required'));
  }
  $error8 = $_POST['filter_2_sql'];
  if ($error8 == "" ) {
    form_set_error('filter_2_sql', t('The sql for filter 2 is required'));
  }
  
  $error9 = $_POST['filter_3_label'];
  if ($error9 == "" ) {
    form_set_error('filter_3_label', t('The label for filter 3 is required'));
  }
  $error10 = $_POST['filter_3_sql'];
  if ($error10 == "" ) {
    form_set_error('filter_3_sql', t('The sql for filter 3 is required'));
  }
  
  $error11 = $_POST['filter_4_label'];
  if ($error11 == "" ) {
    form_set_error('filter_4_label', t('The label for filter 4 is required'));
  }
  $error12 = $_POST['filter_4_sql'];
  if ($error12 == "" ) {
    form_set_error('filter_4_sql', t('The sql for filter 4 is required'));
  }

  
  $error13 = $_POST['title_link_bottom'];
  if ($error13 == "" ) {
    form_set_error('title_link_bottom', t('The title for bottom link is required'));
  }
  $error14 = $_POST['url_link_bottom'];
  if ($error14 == "" ) {
    form_set_error('url_link_bottom', t('The url for bottom link is required'));
  }
  
  $error15 = $_POST['query'];
  if ($error15 == "" ) {
    form_set_error('query', t('The query is required'));
  }
 /* $error16 = $_POST['fields'];
  if ($error16 == "" ) {
    form_set_error('fields', t('Fields is required'));
  }*/
}

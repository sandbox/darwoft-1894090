<div class="selective-content-block-wrapper">
    <table style="float:left; border-style:none; border-collapse:collapse;" >
        <tr>
            <td>
                <div class="selective_content_block-title"><?php echo $title ?></div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="selective_content_block-title-link"><a href="<?php echo $urlLink ?>" target="_self"><?php echo $titleLink ?></a></div>
            </td>
        </tr>
        <tr>
            <td>
                 <div class="selective_content_block-text"><?php echo $intro ?></div>
            </td>
        </tr>
    </table>

    <div>
         <div  id="selective_content_block-filter-1" class="selective_content_block-filters"><?php echo $filterLabel1 ?></div>
         <div class="selective_content_block-separator">&nbsp;&nbsp;|&nbsp;&nbsp;</div>
         <div id="selective_content_block-filter-2" class="selective_content_block-filters"><?php echo $filterLabel2 ?></div>
         <div class="selective_content_block-separator">&nbsp;&nbsp;|&nbsp;&nbsp;</div>
         <div  id="selective_content_block-filter-3" class="selective_content_block-filters"><?php echo $filterLabel3 ?></div>
         <div class="selective_content_block-separator">&nbsp;&nbsp;|&nbsp;&nbsp;</div>
         <div id="selective_content_block-filter-4" class="selective_content_block-filters"><?php echo $filterLabel4 ?></div>
    </div>
    <!--<table style="float:left; border-style:none; border-collapse:collapse;" >
        <tr>
            <td>
                <div  id="selective_content_block-filter-1" class="selective_content_block-filters"><?php echo $filterLabel1 ?></div>
            </td>
            <td>
                <div class="selective_content_block-separator">|</div>
            </td>
            <td>
                <div id="selective_content_block-filter-2" class="selective_content_block-filters"><?php echo $filterLabel2 ?></div>
            </td>
            <td>
                <div class="selective_content_block-separator">|</div>
            </td>
            <td>
                <div  id="selective_content_block-filter-3" class="selective_content_block-filters"><?php echo $filterLabel3 ?></div>
            </td>
            <td>
                <div class="selective_content_block-separator">|</div>
            </td>
            <td>
                <div id="selective_content_block-filter-4" class="selective_content_block-filters"><?php echo $filterLabel4 ?></div>
            </td>
        </tr>

    </table>-->
    <div id="selective_content_block_loading" style="width:100%;display:none;text-align: center;height: 100px;">
          <img style="padding-top:30px;padding-bottom:30px;" src="<?php echo $GLOBALS['base_path'] . drupal_get_path('module', 'selective_content_block')?>/images/load.gif">
    </div>
    <div id="selective_content_block-body-find">
          <?php echo $cuerpoBlock ?>
    </div>
    <table style="float:left; border-style:none; border-collapse:collapse;  table-layout:fixed; width:100%;" >
        <tr>
            <td>
                <div class="selective_content_block-bottom-link"><a href="<?php echo $urlBottomLink ?>" target="_self"><?php echo $titleBottomLink ?></a></div>
            </td>
        </tr>

    </table>
</div>